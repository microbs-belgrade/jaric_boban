import { useState, useEffect } from 'react';
import './App.css';
import datas from './datas.json'

const helperFunc = (data) => {
  const result = data.map(result => result.collectionName).sort((a, b) => (a > b) ? 1 : -1);
  const corrData = [...new Set(result)];
  const datas = corrData.splice(0, 5);
  return datas;
};

function App() {
  const [list, setList] = useState(['A', 'B', 'C', 'D', 'E']);
  const [listItem, setListItem] = useState(0);
  const [finalData, setFinalData] = useState([]);

  useEffect(() => {
    const interval = setInterval(() => {

      if (finalData.length > 0 && listItem < 5) {
        const listCorr = [...list];
        listCorr.shift();
        listCorr.unshift(finalData[listItem])
        const shitedItem = listCorr.shift();
        listCorr.push(shitedItem);
        setListItem(listItem + 1);
        setList(listCorr);
      } else {
        const listCorr = [...list];
        const shitedItem = listCorr.shift();
        listCorr.push(shitedItem);
        setList(listCorr);
      }
    }
      , 1000);
    return () => {
      clearInterval(interval);
    };
  }, [list, finalData, listItem]);

  const fetchItem = async (e) => {

    let nameSearch = e.target.value.toLowerCase();
    if (nameSearch === 'radiohead') {
      setFinalData(helperFunc(datas.results))
      /*       try {
             const responseData = await fetch(`https://itunes.apple.com/search?term=${nameSearch}`, {
               mode: 'no-cors',
               headers: {
                 'Access-Control-Allow-Origin': '*'
               }
             });
             console.log(responseData);
              setFinalData(helperFunc(responseData.results)) 
           } catch (err) {
     
           }  */

    }
  };



  return (
    <div id="main-div">
      <div id='input-holder'>
        <input
          type="search"
          autoComplete="off"
          onChange={(e) => fetchItem(e)}
          placeholder="Search" />
        <div id='list'>
          {list.map((item, index) => (
            <div key={index} className='list-element'>
              <p>{item}</p>
            </div>
          ))
          }
        </div>
      </div>
    </div>
  )


}

export default App;
